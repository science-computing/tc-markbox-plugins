try:
    from setuptools import setup, find_packages
except ImportError:
    from distutils.core import setup
setup(name='testcreator-markbox-plugins',
    version='0.1',
    description='Importer / Exporter for Testcreator / Markbox1/2 Interop',
    #author='Ryan Goggin',
    #author_email='ryan.goggin@uwaterloo.ca',
    #url='https://ryangoggin.net',
    packages=find_packages(),
    include_package_data=True,
)

