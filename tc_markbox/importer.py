import html
import json
import re
import zipfile

from testcreator.importers.base import BaseImporter


def katex_convert(eq):
    eq = eq.groups()[0]
    return '<span equation-render="true" data-raw="{}"></span>'.format(eq)


class MarkboxLegacyImporter(BaseImporter):
    name = "Markbox (Legacy) Importer"
    slug = "markbox_legacy"
    detail = "Import .test files from the original MarkBox website"
    extensions = [".test"]
    notices = ["Currently does not support Feedback (coming soon)"]
    supported_question_types = {"native_mc": {}}

    def save_image(self, img_match):
        img_path = img_match.groups()[0]
        if img_path.endswith('/'):
            img_path = img_path[:-1]
        saved_img = self.create_image(self.testfile.open(img_path))
        return 'src="/image/view/{}"'.format(saved_img.id)
        raise Exception('pp')


    def parse_html(self, html_data):
        if not html_data:
            return ''
        html_data = html_data.replace('\n', '')
        html_data = html_data.replace('&nbsp;', ' ')
        html_data = re.sub(
            r'<style>(.*?)<\/style>',
            '',
            html_data
        )
        html_data = re.sub(
            r'<style>(.*?)</style>',
            '',
            html_data
        )
        # At this point we should be able to run self.create_image
        html_data = re.sub(
            r'\<img title="(.*?)".*?codecogs.*?\"\>', katex_convert, html_data
        )

        html_data = re.sub(
            r"src=\"\/static\/(.*?)\"",
            self.save_image,
            html_data,
        )
        li = html_data.rsplit('<br>', 1)
        if len(li) > 1 and len(li[1]) < 7:
            return ''.join(li)
        return html_data

    def parse_question(self, q_data):
        output = dict(
            max_value=float(q_data.get("scoring")),
            config=dict(shuffle=q_data.get("shuffle")),
            question=dict(type="native_mc", feedback={}, solution={},),
        )

        feedback = dict(
            answers=[{} for _ in q_data.get("answer_set", [])]
        )
        body = dict(
            content=self.parse_html(q_data["text"]),
            answers=[
                dict(
                    correct=a.get("correct", False),
                    content=self.parse_html(a.get("text")),
                )
                for a in q_data.get("answer_set", [])
            ],
        )

        output["question"].update(body=body, feedback=feedback)
        return output

    def parse_group(self, group_data):
        output = dict(
            content=self.parse_html(group_data.get("text", '')),
            questions=[],
            shuffle=group_data["shuffle"],
        )

        title = group_data.get("name", "group_name")
        if title == "group_name":
            title = ""
        output["title"] = title
        if group_data.get("showname", False) and title != "":
            output["content"] = "<h2>{}</h2>".format(title) + output["content"]

        for question in group_data["question_set"]:
            if not question.get("trash"):
                output["questions"].append(self.parse_question(question))

        return output

    def json_to_local(self, data):
        print(data)
        test = dict(
            title=data.get("title", 'Untitled'),
            coversheet=self.parse_html(data["text"]),
            groups=[],
        )

        for group in data["questiongroup_set"]:
            if not group.get("trash", False):
                test["groups"].append(self.parse_group(group))

        json.dumps(test, indent=4)
        #raise Exception("failing on purpose")
        return test

    def convert(self):
        with zipfile.ZipFile(self.file_data) as testfile:
            self.testfile = testfile
            with testfile.open("test.json") as c:
                jdata = json.load(c)
            test_data = self.json_to_local(jdata)
        test_data.update(trash=[])
        return test_data
