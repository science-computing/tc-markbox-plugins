import io
import json
import re
import zipfile
from uuid import uuid4

from testcreator.exporters.base import BaseExporter
from testcreator.exporters.utilities import (
    HtmlProcessor,
    convert_equations_to_katex,
    standard_html_styles,
)


class Processor(HtmlProcessor):
    pre_process = [
        lambda x: re.sub(
            r"src=\"\/image\/view\/(.*?)\"",
            r'src="file://media/\1.png" style="max-width: 100%"',
            x,
        ),
    ]
    bs4_process = [
        standard_html_styles,
        convert_equations_to_katex,
    ]


def cleaner(htmlstr):
    return Processor(htmlstr).process()


class MarkboxExporter(BaseExporter):
    slug = "markbox"
    name = "Markbox Test"
    content_type = "application/zip"
    extension = "test"
    detail = "Export a Markbox v2 Compatible .test file"
    notices = ["Does not currently support exporting SOLUTIONS or FEEDBACK"]
    supported_question_types = {"native_mc": {}}

    def export_test(self):
        test = self.test
        self.uuid = str(uuid4())
        self.test_data = {
            "uid": self.uuid,
            "page_size": "letter",
            "cover_sheet": {"content": cleaner(test.coversheet), "title": ''},
            "feedback": {},
            "question_groups": [],
        }

    def export_group(self, group, gi):
        g = {
            "ident": f"SECT_{self.uuid}_{gi}",
            "title": "",
            "content": cleaner(group.content),
            "scramble_questions": group.shuffle,
            "feedback": {},
            "questions": [],
        }
        self.current_group = g
        self.test_data["question_groups"].append(g)

    def export_question_instance(self, instance, iindex):
        guid = self.current_group["ident"]
        q = {
            "ident": f"QUES_{guid}_{iindex}",
            "global_id": str(uuid4()),
            "title": "",
        }
        self.current_group["questions"].append(q)
        self.current_q = q

    def get_file(self):
        outfile = io.BytesIO()
        with zipfile.ZipFile(outfile, "w") as z:
            z.writestr("test.json", json.dumps(self.test_data))
            z.writestr("version", "3")

            for image in self.test.image_set.all():
                z.write(image.file.path, "uploads/{}.png".format(image.id))
        outfile.seek(0)
        return outfile

    def export_native_mc(self, question, instance, *args, **kwargs):
        body = question.body
        config = instance.config
        quid = self.current_q["ident"]
        self.current_q.update(
            {
                "content": cleaner(body["content"]),
                "scramble_answers": config["shuffle"],
                "fix_order": instance.pinned,
                "answers": [
                    {
                        "ident": f"{quid}_{ai}",
                        "correct": a["correct"],
                        "content": cleaner(a["content"]),
                        "fix_order": a["pinned"],
                    }
                    for ai, a in enumerate(body["answers"])
                ],
                "scoring": {
                    "correct": float(instance.max_value),
                    "incorrect": 0,
                    "partial": False,
                    "num_correct": 1,
                },
            }
        )


"""
    conf_dir = clean_old(markset)
    media_dir = conf_dir / "media"
    # First let's copy all the media files if there are any...
    for zinfo in testfile.infolist():
        if "upload" in zinfo.filename:
            # Cut all the crap out of the filenames...
            filename = zinfo.filename.split("/")[-1]
            with open(media_dir / filename, "wb") as f:
                f.write(testfile.read(zinfo))
    # Next lets parse the actual json file...
    with testfile.open("test.json") as c:
        jc = json.load(c)
    # Unpack the JSON config into the new format...
    config = {
        "page_size": "letter" if jc["letterpaper"] else "legal",
        "cover_sheet": {
            "content": v2_cleaner(jc["text"]),
            "title": jc["title"],
        },
        "feedback": {},
        "question_groups": [
            {
                "title": ""
                if (
                    qg["name"] == "group_name" or not qg.get("showname", False)
                )
                else qg["name"],
                "content": v2_cleaner(qg["text"]),
                "scramble_questions": qg["shuffle"],
                "feedback": {},
                "questions": [
                    {
                        "title": ""
                        if q["name"] == "Question name"
                        else q["name"],
                        "content": v2_cleaner(q["text"]),
                        "scramble_answers": q["shuffle"],
                        "fix_order": False,
                        "answers": [
                            {
                                "correct": a["correct"],
                                "content": v2_cleaner(a["text"]),
                                "fix_order": False,
                            }
                            for a in q["answer_set"]
                        ],
                        "scoring": {
                            "correct": 1,
                            "incorrect": 0,
                            "partial": False,
                            "num_correct": 1,
                        },
                    }
                    for q in qg["question_set"]
                    if not q.get("trash", False)
                ],
            }
            for qg in jc["questiongroup_set"]
            if not qg.get("trash", False)
        ],
    }
    return config
"""
